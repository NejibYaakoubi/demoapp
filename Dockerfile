# Utiliser l'image de base node:16-alpine3.12
FROM node:16-alpine3.12 AS builder

# Définir le répertoire de travail
WORKDIR /app

# Copier les fichiers de l'application dans le conteneur
COPY . .

# Installer les dépendances
RUN npm install --force

# Compiler l'application
RUN npm run build -- --outputPath=./dist/out --configuration=production

# Utiliser l'image nginx:alpine comme image finale
FROM nginx:alpine

# Copier les fichiers de l'application compilée dans le répertoire d'nginx
COPY --from=builder /app/dist/out /usr/share/nginx/html

# Exposer le port 80 pour accéder à l'application
EXPOSE 80

# Commande de démarrage pour Nginx
CMD ["nginx", "-g", "daemon off;"]
